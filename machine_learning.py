# machine_learning.py

from joblib import dump
from sklearn import svm
from sklearn import datasets


def train_model():
	print("train model")
	model = svm.SVC()
	X, y = datasets.load_digits(return_X_y=True)
	model.fit(X, y)
	return model

def export_model(model):
	print("export model")
	dump(model, './digits_model.joblib')

def start():
	print("start")
	model = train_model()
	export_model(model)
	print('Model successfully exported.')

if __name__ == '__main__':
	start()

